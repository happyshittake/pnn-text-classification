import json
from collections import Counter
from pathlib import Path
from typing import List, Tuple

import matplotlib.pyplot as plt
import pandas
import xlrd
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from neupy.algorithms import PNN
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import accuracy_score, hamming_loss
from sklearn.model_selection import learning_curve, KFold

k = 5

labels = {
    '1': "Politik",
    '2': "Hukum",
    '3': "Ekonomi",
    '4': "Sosial",
    '5': "Budaya",
    '6': "Teknologi",
    '7': "Gaya Hidup",
    '8': "Olahraga",
    '9': "Entertainment",
    '10': "Penndidikan",
    '11': "HanKam",
    '12': "Kesehatan",
    '13': "Lainnya"
}


def get_data(b):
    d: List[Tuple[str, str]] = []
    sh = b.sheet_by_index(0)
    for r in range(0, sh.nrows, 1):
        d.append((sh.cell_value(r, 1), str(sh.cell_value(r, 2))))

    return d


def clean_data(d: List[Tuple[str, str]]):
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    l: List[str] = []
    t: List[str] = []
    for idx, v in enumerate(d):
        temp = stemmer.stem(v[0].replace('\n', "").replace("\"", "").lower())
        print(temp)
        tags = '&'.join(sorted(list(map(lambda x: str(int(float(x))), v[1].split(",")))))
        l.append(tags)
        t.append(temp)

    return t, l


def plot_learning_curve(estimator, title, X, y, ylim=None, cv=None,
                        n_jobs=1, train_sizes=pandas.np.linspace(.1, 1.0, 5)):
    """
       Generate a simple plot of the test and traning learning curve.

       Parameters
       ----------
       estimator : object type that implements the "fit" and "predict" methods
           An object of that type which is cloned for each validation.

       title : string
           Title for the chart.

       X : array-like, shape (n_samples, n_features)
           Training vector, where n_samples is the number of samples and
           n_features is the number of features.

       y : array-like, shape (n_samples) or (n_samples, n_features), optional
           Target relative to X for classification or regression;
           None for unsupervised learning.

       ylim : tuple, shape (ymin, ymax), optional
           Defines minimum and maximum yvalues plotted.

       cv : integer, cross-validation generator, optional
           If an integer is passed, it is the number of folds (defaults to 3).
           Specific cross-validation objects can be passed, see
           sklearn.cross_validation module for the list of possible objects

       n_jobs : integer, optional
           Number of jobs to run in parallel (default 1).
           :param train_sizes:
       """
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training examples")
    plt.ylabel("Score")
    train_sizes, train_scores, test_scores = learning_curve(
        estimator, X, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes, scoring='accuracy')
    train_scores_mean = pandas.np.mean(train_scores, axis=1)
    train_scores_std = pandas.np.std(train_scores, axis=1)
    test_scores_mean = pandas.np.mean(test_scores, axis=1)
    test_scores_std = pandas.np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
             label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label="Cross-validation score")

    plt.legend(loc="best")
    return plt


def display_scores(vectorizer, tfidf_result):
    # http://stackoverflow.com/questions/16078015/
    scores = zip(vectorizer.get_feature_names(),
                 pandas.np.asarray(tfidf_result.sum(axis=0)).ravel())
    sorted_scores = sorted(scores, key=lambda x: x[1], reverse=True)
    for item in sorted_scores:
        print("{0:50} Score: {1}".format(item[0], item[1]))


if __name__ == "__main__":
    dataset_file = Path('./data_frame.json')
    datatest_file = Path('./data_test.json')

    stop_words = []
    with open('stopwords-id.txt') as f:
        for line in f:
            val = line.lower().split()
            stop_words.append(val[0])

    data_frame = pandas.DataFrame()
    test_data_frame = pandas.DataFrame()

    if not dataset_file.is_file():
        book = xlrd.open_workbook('dataset_berita.xls')
        item_count = book.sheet_by_index(0).nrows - 1
        data = get_data(book)
        data_frame['texts'], data_frame['labels'] = clean_data(data)
        data_frame.to_json('data_frame.json')
    else:
        with open('./data_frame.json') as dataset_file:
            json_data = json.load(dataset_file)
            data_frame = data_frame.from_records(json_data)

    if not datatest_file.is_file():
        book = xlrd.open_workbook('data_test.xlsx')
        item_count = book.sheet_by_index(0).nrows - 1
        data = get_data(book)
        test_data_frame['texts'], test_data_frame['labels'] = clean_data(data)
        test_data_frame.to_json('data_test.json')
    else:
        with open('./data_test.json') as dataset_file:
            json_data = json.load(dataset_file)
            test_data_frame = data_frame.from_records(json_data)

    tfidf_vect = TfidfVectorizer(analyzer='word', min_df=5, stop_words=stop_words,
                                 sublinear_tf=True, token_pattern='(?u)\\b[a-zA-Z]\\w{2,}\\b', norm='l2')

    kf = KFold(n_splits=5)

    X_train = []
    Y_train = []
    result = next(kf.split(X=data_frame), None)
    tfidf_vect.fit(data_frame.iloc[result[0]]['texts'])
    xtrain_tfidf = tfidf_vect.transform(data_frame.iloc[result[0]]['texts'])

    print("frekunsi kata")
    print("=============================")
    # print(data_frame.iloc[result[0]]['texts'])
    for idx, row in enumerate(data_frame.iloc[result[0]]['texts']):
        print("text ke %d" % idx)
        splitted = row.split(' ')
        counts = Counter(splitted)
        print(counts)

    # pandas.set_option('display.height', 1000)
    pandas.set_option('display.max_rows', 500)
    pandas.set_option('display.max_columns', 500)
    pandas.set_option('display.width', 1000)
    corpus_idx = [n for n in data_frame.iloc[result[0]]['texts'].keys()].sort()
    df = pandas.DataFrame(xtrain_tfidf.T.todense(), index=tfidf_vect.get_feature_names(), columns=corpus_idx)
    print(df)

    plotData = pandas.DataFrame()
    plotData['train_data_label_count'] = data_frame.iloc[result[0]].groupby('labels').texts.count()
    plotData['test_data_label_count'] = test_data_frame.groupby('labels').texts.count()
    fig = plt.figure()
    plt.title("Data distribution")
    plotData.plot.bar(ylim=0)
    plt.show()

    # feature_array = pandas.np.array(tfidf_vect.get_feature_names())
    # tfidf_sorting = pandas.np.argsort(xtrain_tfidf.toarray()).flatten()[::-1]
    # n = 3
    # top_n = feature_array[tfidf_sorting][:n]
    # # print(top_n)
    # encoder = preprocessing.LabelEncoder()
    # train_y = encoder.fit_transform(data_frame['labels'].values.tolist())
    # valid_y = encoder.fit_transform(test_data_frame['labels'].values.tolist())

    pnn_network = PNN(std=0.3)
    pnn_network.fit(xtrain_tfidf.toarray(), data_frame.iloc[result[0]]['labels'].values)
    print(test_data_frame['labels'])
    predict_arr = []
    valid_arr = []
    for idx, row in test_data_frame.iterrows():
        print(row['texts'])
        print(row['labels'])
        valid_arr.append(row['labels'])
        predicted = pnn_network.predict(tfidf_vect.transform([row['texts']]).toarray())
        print(predicted)
        predict_arr.extend(predicted)
    # print(test_data_frame['labels'].values)

    print("akurasi %f" % accuracy_score(predict_arr, valid_arr))
    print("hamming loss %f" % hamming_loss(predict_arr, valid_arr))
